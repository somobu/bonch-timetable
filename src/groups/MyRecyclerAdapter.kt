package com.somobu.timetable.groups

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.somobu.timetable.R


class MyRecyclerAdapter(private val list: ArrayList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var clickListener: AdapterView.OnItemClickListener? = null

    inner class ViewHolderSec(view: View) : RecyclerView.ViewHolder(view) {
        var text = view.findViewById(R.id.text) as TextView
    }

    inner class ViewHolderBtn(view: View) : RecyclerView.ViewHolder(view) {
        var text = view.findViewById(R.id.text) as Button
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].startsWith("C")) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 0) {
            return ViewHolderSec(
                LayoutInflater.from(parent.context).inflate(R.layout.item_groupsec, parent, false)
            )
        } else {
            return ViewHolderBtn(
                LayoutInflater.from(parent.context).inflate(R.layout.item_groupbtn, parent, false)
            )
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderSec) {
            holder.text.text = list[position].substring(1)

            val bgRes =
                if (position == 0) R.drawable.event_header_bg_notop
                else if (position == list.size - 1) R.drawable.event_header_bg_nobottom
                else R.drawable.event_header_bg
            holder.text.setBackgroundResource(bgRes)
        } else if (holder is ViewHolderBtn) {
            holder.text.text = list[position]
            holder.text.setOnClickListener {
                clickListener?.onItemClick(null, null, position, getItemId(position))
            }
        }
    }
}
