package com.somobu.timetable.groups

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.somobu.timetable.R
import com.somobu.sutmodel.GroupsJson
import com.somobu.timetable.AbsOverlayFragment

open class GroupPickerFragment : AbsOverlayFragment() {

    interface OnGroupPickedListener {
        fun onGroupPicked(faculty: String, group: String)
    }

    private val groupsVM: GroupsVM by activityViewModels()

    var pickListener: OnGroupPickedListener? = null

    override fun onCreateContent(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.content_group_picker, container, false)
    }

    override fun onResume() {
        super.onResume()

        groupsVM.groups().observe(this) { groups: GroupsVM.GroupsData ->
            if (groups.isLoaded) {
                if (groups.data != null) onEventsLoaded(groups.data!!)
                if (groups.e != null) setError(groups.e)
            } else {
                setPleaseWait()
            }
        }
    }

    override fun onRetry() {
        groupsVM.loadGroups()
    }

    private fun onEventsLoaded(data: GroupsJson) {
        setContent()

        val root = view ?: return
        val spinner = root.findViewById<Spinner>(R.id.spinner)
        val spinnerIds = arrayListOf<String>()
        val spinnerData = arrayListOf<String>()
        for (item in data.order) {
            spinnerIds.add(item.key)
            spinnerData.add(data.faculties[item.key]!!)
        }
        spinner.adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, spinnerData)
        spinner.onItemSelectedListener = object : OnItemSelectedListener {

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, i: Int, p3: Long) {
                showGroupNames(data, spinnerIds[i])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
        spinner.setSelection(0)
    }

    private fun showGroupNames(data: GroupsJson, faculty: String) {
        val root = view ?: return

        val list = root.findViewById<RecyclerView>(R.id.list)

        val listIds = arrayListOf<String>()
        val listData = arrayListOf<String>()
        for (course in data.order[faculty]!!) {
            listIds.add("")
            listData.add("C" + getString(R.string.group_picker_course_divider, course.key))
            for (group in course.value) {
                listIds.add(group)
                listData.add(data.groups[group]!!)
            }
        }

        // Final section, w/o any title
        listIds.add("")
        listData.add("C")

        val displayMetrics: DisplayMetrics = requireContext().resources.displayMetrics
        val dpWidth: Float = displayMetrics.widthPixels / displayMetrics.density
        val btnCount = (dpWidth / 96).toInt()

        val manager = GridLayoutManager(requireContext(), btnCount)
        manager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (listIds[position] == "") btnCount else 1
            }
        }
        list.layoutManager = manager

        val adapter = MyRecyclerAdapter(listData)
        adapter.clickListener = OnItemClickListener { _, _, i, _ ->
            pickListener?.onGroupPicked(faculty, listIds[i])
        }
        list.adapter = adapter

    }
}