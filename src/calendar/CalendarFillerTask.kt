package com.somobu.timetable.calendar

import android.app.AlertDialog
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.os.Handler
import android.provider.CalendarContract
import com.google.gson.GsonBuilder
import com.somobu.timetable.R
import com.somobu.timetable.API
import com.somobu.timetable.timetables.Event
import com.somobu.timetable.timetables.MyRecyclerAdapter
import com.somobu.timetable.timetables.PairsToEvents
import java.util.*


class CalendarFillerTask(
    private val context: Context,
    private val calendar: CalendarInfo,
    mode: FillMode,
    private val group: String
) : Thread() {

    enum class FillMode {
        TWO_WEEKS,
        ALL_EVENTS
    }

    private val handler: Handler = Handler()
    private var blockingDialog: AlertDialog? = null

    private val since: Long
    private val to: Long

    private val currentEvents: ArrayList<CalendarEvent>

    init {
        if (mode == FillMode.TWO_WEEKS) {
            // Get calendar set to current date and time
            val c = Calendar.getInstance()
            c.clear(Calendar.MILLISECOND)
            c.clear(Calendar.SECOND)
            c.clear(Calendar.MINUTE)
            c[Calendar.HOUR_OF_DAY] = 0 // Cuz clear would not reset the hour of day
            c[Calendar.DAY_OF_WEEK] = c.firstDayOfWeek // Set the calendar to first day of the current week

            since = c.timeInMillis
            c.add(Calendar.WEEK_OF_YEAR, 2)
            to = c.timeInMillis

        } else {
            since = 0
            to = Long.MAX_VALUE
        }

        currentEvents = getCurrentEvents()
        handler.post { onPreExecute() }
    }

    private fun getCurrentEvents(): ArrayList<CalendarEvent> {
        val list: ArrayList<CalendarEvent> = ArrayList<CalendarEvent>()
        val projection = arrayOf(
            CalendarContract.Events._ID,
            CalendarContract.Events.CALENDAR_ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.CUSTOM_APP_PACKAGE
        )

        val selection = ("( " + CalendarContract.Events.CALENDAR_ID + " == " + calendar.id
                + " ) AND ( " + CalendarContract.Events.DTSTART + " >= " + since
                + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + to
                + " ) AND ( " + CalendarContract.Events.CUSTOM_APP_PACKAGE + " == \"" + context.packageName + "\""
                + " ) AND ( deleted != 1 )")

        val cursor = context.contentResolver
            .query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null)

        if (cursor != null && cursor.count > 0 && cursor.moveToFirst()) {
            do {
                list.add(
                    CalendarEvent(
                        cursor.getLong(0),
                        cursor.getLong(1),
                        cursor.getString(2),
                        cursor.getLong(3),
                        cursor.getString(4)
                    )
                )
            } while (cursor.moveToNext())
            cursor.close()
        }
        return list
    }

    private fun onPreExecute() {
        blockingDialog = AlertDialog.Builder(context).setMessage(R.string.insertion_in_progress).create()
        blockingDialog!!.setCancelable(false)
        blockingDialog!!.show()
    }

    override fun run() {
        var caughtException: Exception? = null

        try {
            val groups = API.getGroups(context)
            val timetable = API.getGroupTimetable(context, group)

            for (oldEvent in currentEvents) {
                deleteEvent(oldEvent.id)
            }

            for (ev in PairsToEvents.convert(timetable.pairs, groups, timetable.subjects, timetable.teachers)) {
                if (ev is MyRecyclerAdapter.PairEntry) {
                    if (ev.pair.since in since..to) insertOrUpdate(ev.pair)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            caughtException = e
        }

        val finalException = caughtException
        handler.post { onPostExecute(finalException) }
    }

    private fun insertOrUpdate(event: Event) {
        val values = ContentValues()
        values.put(CalendarContract.Events.DTSTART, event.since)
        values.put(CalendarContract.Events.DTEND, event.to)
        values.put(CalendarContract.Events.TITLE, event.title)
        values.put(CalendarContract.Events.DESCRIPTION, event.fancyDescription())
        values.put(CalendarContract.Events.CALENDAR_ID, calendar.id)
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().id)
        values.put(CalendarContract.Events.EVENT_LOCATION, event.location)
        values.put(CalendarContract.Events.CUSTOM_APP_PACKAGE, context.packageName)

        val gson = GsonBuilder().create()
        values.put(CalendarContract.Events.CUSTOM_APP_URI, "event://details?data=" + gson.toJson(event))

        insertEvent(values)
    }

    private fun insertEvent(values: ContentValues) {
        context.contentResolver.insert(CalendarContract.Events.CONTENT_URI, values)
    }


    private fun deleteEvent(eventID: Long) {
        val deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID)
        context.contentResolver.delete(deleteUri, null, null)
    }

    private fun onPostExecute(result: Exception?) {
        blockingDialog!!.dismiss()

        val message: String
        if (result == null) {
            message = context.getString(R.string.insertion_success)
        } else {
            message = context.getString(R.string.insertion_failed, result.localizedMessage)
        }

        AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(R.string.insertion_ok, null)
            .show()
    }
}
