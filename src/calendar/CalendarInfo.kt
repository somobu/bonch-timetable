package com.somobu.timetable.calendar

/**
 * Информация о календаре (который android'овский, из CalendarProvider)
 */
class CalendarInfo(val id: String, val name: String) {
    override fun toString(): String {
        return "$name (#$id)"
    }
}
