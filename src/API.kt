package com.somobu.timetable

import android.content.Context
import com.google.gson.GsonBuilder
import com.somobu.sutmodel.GroupTimetable
import com.somobu.sutmodel.GroupsJson
import com.somobu.sutmodel.NewsEntry
import com.somobu.sutmodel.TeacherTimetable
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

object API {

    private const val srvAddress = "https://bonchdata.kitsuki.cc"
    private const val keepCached = 15 * 60 * 1000L

    private const val debug = false

    private val gson = GsonBuilder().create();
    
    fun getNews(context: Context): Array<NewsEntry> {
        return getNetworkFile(context, "news.json", Array<NewsEntry>::class.java)
    }
    
    fun getGroups(context: Context): GroupsJson {
        return getNetworkFile(context, "groups.json", GroupsJson::class.java)
    }

    fun getTeachers(context: Context): Array<String> {
        return getNetworkFile(context, "teachers_idx.json", Array<String>::class.java)
    }

    fun getGroupTimetable(context: Context, group: String): GroupTimetable {
        return getNetworkFile(context, "by_group/$group.json", GroupTimetable::class.java)
    }

    fun getTeacherTimetable(context: Context, teacher: String): TeacherTimetable {
        return getNetworkFile(context, "by_teacher/$teacher.json", TeacherTimetable::class.java)
    }

    @Synchronized
    private fun <T> getNetworkFile(context: Context, fileName: String, clazz: Class<T>): T {
        val cachedVer = File(context.cacheDir, fileName)
        var useCache = false

        if (cachedVer.exists()) {
            if (System.currentTimeMillis() - cachedVer.lastModified() < keepCached) {
                useCache = true
            }
        }

        var rawJson: String? = null
        var networkException: Exception? = null

        if (useCache) {
            v("Using cached version of $fileName")
            rawJson = readFromDisk(cachedVer)
        } else {
            val url = URL("$srvAddress/$fileName")

            try {
                rawJson = readFromNet(url)
            } catch (e: Exception) {
                networkException = e
            }

            if (rawJson != null) { // We successfully got a file from server

                v("Using network version of $fileName")
                writeToDisk(cachedVer, rawJson)

            } else { // We're unable to get file from the net

                if (cachedVer.exists()) { // Maybe we can read outdated version from disk?
                    try {
                        rawJson = readFromDisk(cachedVer)
                        v("Using old-cached version $fileName")
                    } catch (ignored: Exception) {
                        ignored.printStackTrace()

                        // Something is wrong w/ disk cache
                        // Lets report to user network exception instead
                        throw networkException!!
                    }
                } else { // We can not get file from the net, nor we can read it from disk
                    throw networkException!!
                }

            }
        }

        v("Raw json is:")
        v(rawJson)
        
        return gson.fromJson(rawJson, clazz)!!
    }

    private fun readFromNet(url: URL): String {
        val rawJson: String

        val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
        urlConnection.connectTimeout = 1000
        urlConnection.readTimeout = 1000

        val istream: InputStream?

        try {
            istream = BufferedInputStream(urlConnection.inputStream)
            val s = Scanner(istream).useDelimiter("\\A")
            rawJson = if (s.hasNext()) s.next() else ""
        } finally {
            urlConnection.disconnect()
        }

        return rawJson
    }

    private fun readFromDisk(file: File): String {
        val istream: InputStream = FileInputStream(file)
        val s = Scanner(istream).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }

    private fun writeToDisk(cachedVer: File, rawJson: String) {
        try {
            if (!cachedVer.exists()) {
                cachedVer.parentFile!!.mkdirs()
                cachedVer.createNewFile()
            }

            val os = FileOutputStream(cachedVer)
            val osw = OutputStreamWriter(os)
            osw.write(rawJson)
            osw.flush()
            osw.close()

            cachedVer.setLastModified(System.currentTimeMillis())
        } catch (ignored: Exception) {
            // We dont care if we're unable to cache it
            cachedVer.delete()
        }
    }

    fun digGroupName(groups: GroupsJson, selectedGroup: String): String? {
        var faculty = ""
        val group = groups.groups[selectedGroup] ?: selectedGroup

        for (fac in groups.order) {
            for (course in fac.value) {
                if (course.value.contains(selectedGroup)) {
                    faculty = groups.faculties[fac.key]!!
                    return "$faculty / $group"
                }
            }
        }

        return null
    }

    fun v(s: String) {
        if (debug) println(s)
    }
}