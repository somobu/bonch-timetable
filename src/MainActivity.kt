package com.somobu.timetable

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.somobu.sutmodel.NewsEntry
import com.somobu.timetable.groups.GroupsVM
import com.somobu.timetable.timetables.NewsVM
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashSet

class MainActivity : AppCompatActivity() {

    companion object {

        /**
         * User's group number (shared prefs key)
         */
        const val GROUP_ID = "bonch_group"

    }

    private var appBarConfiguration: AppBarConfiguration? = null
    private var showSubtitle = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val navController = navHostFragment!!.navController
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        appBarConfiguration = AppBarConfiguration.Builder(getTopLevelDestinations())
            .setFallbackOnNavigateUpListener({ onSupportNavigateUp() })
            .setOpenableLayout(drawerLayout)
            .build()
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration!!)

        val navView = findViewById<NavigationView>(R.id.nav_view)
        NavigationUI.setupWithNavController(navView, navController)

        navView.itemIconTintList = null
        navView.setNavigationItemSelectedListener { item: MenuItem ->

            showSubtitle = item.itemId in arrayOf(R.id.timetableListFragment, R.id.insertionFragment)
            pokeSubtitle()

            if (item.itemId == R.id.telegram) {
                val url = getString(R.string.link_telegram)
                openUrl(url)
                return@setNavigationItemSelectedListener false
            } else {
                navController.navigate(item.itemId)
                drawerLayout.closeDrawers()
                return@setNavigationItemSelectedListener true
            }
        }

        // Update subtitle at last
        pokeSubtitle()

        setupNewsBlock()
    }

    private fun getTopLevelDestinations(): MutableSet<Int> {
        val topLevelDestinations: MutableSet<Int> = HashSet()
        topLevelDestinations.add(R.id.timetableListFragment)
        topLevelDestinations.add(R.id.groupTtPickerFragment)
        topLevelDestinations.add(R.id.teachersListFragment)
        topLevelDestinations.add(R.id.insertionFragment)
        topLevelDestinations.add(R.id.aboutFragment)
        return topLevelDestinations
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(this, R.id.nav_host_fragment)
        return NavigationUI.navigateUp(navController, appBarConfiguration!!) || super.onSupportNavigateUp()
    }

    private fun pokeSubtitle() {
        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val selectedGroup = prefs.getString(GROUP_ID, null)

        val groupVM: GroupsVM by viewModels()

        groupVM.groups().observe(this) { groups: GroupsVM.GroupsData ->
            if (groups.isLoaded && groups.data != null) {
                val subtitle = API.digGroupName(groups.data!!, selectedGroup!!)
                if (showSubtitle) supportActionBar?.subtitle = subtitle
                else supportActionBar?.subtitle = ""
            }
        }

    }

    private fun setupNewsBlock() {
        val newsRoot: LinearLayout = findViewById(R.id.news_block)

        val newsVM: NewsVM by viewModels()

        newsRoot.visibility = LinearLayout.GONE

        newsVM.news().observe(this) { news: NewsVM.NewsData ->
            val pref = PreferenceManager.getDefaultSharedPreferences(this).getLong("LAST_NEWS_ENTRY", 0)

            if (news.isLoaded && news.lastEntry != null) {
                var entry = news.lastEntry!!
                
                if (entry.time <= pref) { // i.e. entry should be dismissed
                    if (news.lastNonDismissibleEntry != null) {
                        entry = news.lastNonDismissibleEntry!!
                    } else {
                        return@observe
                    }
                }
                
                val entryTitle = entry.title
                val entryText = entry.message

                newsRoot.findViewById<TextView>(R.id.title).text = entryTitle
                newsRoot.findViewById<TextView>(R.id.subtitle).text = entryText
                newsRoot.findViewById<ImageButton>(R.id.btn_bigger).setOnClickListener(object : OnClickListener {
                    override fun onClick(p0: View?) {
                        showNewsEntryDialog(entry, newsRoot)
                    }
                })

                newsRoot.setOnClickListener(object : OnClickListener {
                    override fun onClick(p0: View?) {
                        showNewsEntryDialog(entry, newsRoot)
                    }
                })

                val dismissBtn = newsRoot.findViewById<ImageButton>(R.id.btn_close);
                if (entry.dismissible) {
                    dismissBtn.visibility = View.VISIBLE
                    dismissBtn.setOnClickListener(object : OnClickListener {
                        override fun onClick(p0: View?) {
                            newsRoot.visibility = LinearLayout.GONE
                            hideEntry(entry)
                            setupNewsBlock()
                        }
                    })
                } else {
                    dismissBtn.visibility = View.GONE
                }

                newsRoot.visibility = LinearLayout.VISIBLE
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun showNewsEntryDialog(data: NewsEntry, newsRoot: View) {
        val entryTitle = data.title
        val entryTime = SimpleDateFormat("dd.MM HH:mm").format(Date(data.time))
        val entryText = "$entryTime - ${data.message}"

        val ab = AlertDialog.Builder(this@MainActivity)
            .setTitle(entryTitle)
            .setMessage(entryText)
            .setCancelable(true)
            .setPositiveButton("Ок", null)

        if (data.dismissible) {
            ab.setNegativeButton("Скрыть", object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    newsRoot.visibility = LinearLayout.GONE
                    hideEntry(data)
                    setupNewsBlock()
                }
            })
        }

        if (data.url != null) {
            ab.setNeutralButton(
                data.url.label,
                object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        openUrl(data.url.url)
                    }
                })
        }

        ab.show()
    }

    private fun hideEntry(data: NewsEntry) {
        PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
            .edit()
            .putLong("LAST_NEWS_ENTRY", data.time)
            .apply()
    }

    private fun openUrl(url: String) {
        try {
            val uri = Uri.parse(url)
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (e1: ActivityNotFoundException) {
            Toast.makeText(
                this@MainActivity,
                getString(R.string.unable_to_pick_activity, url),
                Toast.LENGTH_LONG
            ).show()
            e1.printStackTrace()
        }
    }
}