package com.somobu.timetable.timetables

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.somobu.timetable.R
import com.somobu.timetable.groups.GroupPickerFragment

class GroupTtPickerFragment : GroupPickerFragment(), GroupPickerFragment.OnGroupPickedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickListener = this
    }

    override fun onGroupPicked(faculty: String, group: String) {
        val bundle = Bundle()
        bundle.putString("group", group)
        findNavController().navigate(R.id.action_groupTtPickerFragment_to_groupTtFragment, bundle)
    }

}