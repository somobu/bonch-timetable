package com.somobu.timetable.timetables

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayout
import com.somobu.timetable.R
import com.somobu.sutmodel.Pair
import java.text.SimpleDateFormat
import java.util.*

class MyRecyclerAdapter(
    private val list: List<Entry>,
    private val onBadgeClick: (Int) -> Unit,
    private val onTeacherClick: (String) -> Unit,
    private val hideSingleGroup: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    open class Entry(val date: Long)

    class DividerEntry(date: Long, val isEndEntry: Boolean) : Entry(date)

    class PairEntry(val pair: Event) : Entry(pair.since)


    @SuppressLint("SimpleDateFormat")
    private var dateFormat = SimpleDateFormat("dd.MM, EEEE")

    @SuppressLint("SimpleDateFormat")
    private var timeFormat = SimpleDateFormat("HH:mm")

    val DIVIDER = 0
    val EVENT = 1


    inner class DivierViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val divider: TextView = view.findViewById(R.id.divider)
    }

    inner class PairViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bg: View = view.findViewById(R.id.event_bg)
        val title: TextView = view.findViewById(R.id.title)

        val badgeDate: TextView = view.findViewById(R.id.badge_date)
        var badgeType: TextView = view.findViewById(R.id.badge_type)
        val badgeLoc: TextView = view.findViewById(R.id.badge_loc)

        val groups: FlexboxLayout = view.findViewById(R.id.groups)
        val teachers: FlexboxLayout = view.findViewById(R.id.teachers)

        val badgeSpace = view.resources.getDimension(R.dimen.badge_space).toInt()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == DIVIDER) {
            return DivierViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_event_div, parent, false)
            )
        } else {
            return PairViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false)
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = list[position]

        if (item is DividerEntry) {
            return DIVIDER;
        } else {
            return EVENT;
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == DIVIDER) {
            bindDivider(holder as DivierViewHolder, position)
        } else {
            bindPair(holder as PairViewHolder, position)
        }

    }

    fun bindDivider(holder: DivierViewHolder, position: Int) {
        val item = list[position] as DividerEntry
        val curr = item.date

        if (item.isEndEntry) {
            holder.divider.text = "\n\nВот и конец расписания\n\n"
        } else {
            holder.divider.text = dateFormat.format(Date(curr))
        }
    }

    fun bindPair(holder: PairViewHolder, position: Int) {
        val curr: Event = (list[position] as PairEntry).pair

        var prev: Entry? = null
        if (position > 0) prev = list[position - 1]

        var next: Entry? = null
        if (position + 1 < list.size) next = list[position + 1]

        val context = holder.badgeLoc.context

        // Current event highlight
        val bgRes = if (curr.isOngoing()) {
            if (prev is DividerEntry && next is DividerEntry) {
                R.drawable.event_ongoing_bg   
            } else if (prev is DividerEntry) {
                R.drawable.event_ongoing_nobottom
            } else if (next is DividerEntry) {
                R.drawable.event_ongoing_notop
            } else {
                R.color.ongoing_event
            }
        } else {
            if (prev is DividerEntry && next is DividerEntry) {
                R.drawable.event_normal_central
            } else if (prev is DividerEntry) {
                R.drawable.event_normal_top
            } else if (next is DividerEntry) {
                R.drawable.event_normal_bottom
            } else {
                R.color.white_card
            }
        }
        holder.bg.setBackgroundResource(bgRes)

        // Title
        holder.title.text = curr.title

        // 1st level badges
        val time = timeFormat.format(Date(curr.since)) + " - " + timeFormat.format(Date(curr.to))
        val pairPrefix = if (curr.pairName.isNotEmpty()) "${curr.pairName} | " else ""
        holder.badgeDate.text = "$pairPrefix$time"

        val type = when (curr.type) {
            Pair.Type.LECT -> context.getString(R.string.pair_type_lect)
            Pair.Type.PRACT -> context.getString(R.string.pair_type_pract)
            Pair.Type.LABA -> context.getString(R.string.pair_type_laba)
            Pair.Type.ZACH -> context.getString(R.string.pair_type_zach)
            Pair.Type.CONS -> context.getString(R.string.pair_type_cons)
            Pair.Type.EXAM -> context.getString(R.string.pair_type_exam)
            else -> context.getString(R.string.pair_type_other)
        }
        holder.badgeType.text = type

        holder.badgeLoc.text = curr.location
        holder.badgeLoc.setOnClickListener({})
        if (curr.location.contains("/") && curr.location.matches(Regex("[0-9/\\-].*"))) {
            val split = curr.location.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            var arg = "k" + split[split.size - 1] + "-"
            arg += split[0]

            if (split.size == 3) arg += "a" + split[2]

            holder.badgeLoc.setBackgroundResource(R.drawable.badge_bg_active)
            holder.badgeLoc.setOnClickListener({
                launchInCustomTab(context, "https://nav.sut.ru/?cab=$arg")
            })
        } else {
            holder.badgeLoc.setBackgroundResource(R.drawable.badge_bg_inactive)
            holder.badgeLoc.setOnClickListener({})
        }

        // 2nd level badges
        if (hideSingleGroup && curr.groupIds.size < 2) {
            holder.groups.visibility = LinearLayout.GONE
        } else {
            holder.groups.visibility = LinearLayout.VISIBLE

            for (i in 0 until curr.groupIds.size) {
                val tv: TextView
                if (i < holder.groups.childCount) {
                    tv = holder.groups.getChildAt(i) as TextView
                } else {
                    tv = TextView(context, null, R.attr.groupBadgeStyle)
                    holder.groups.addView(tv)

                    val lp = tv.layoutParams as FlexboxLayout.LayoutParams
                    lp.rightMargin = holder.badgeSpace
                    lp.topMargin = holder.badgeSpace
                    tv.layoutParams = lp
                }

                val groupIdx = curr.groupIds[i]
                tv.text = curr.groups[i]
                tv.setOnClickListener { onBadgeClick(groupIdx) }
            }

            while (holder.groups.childCount > curr.groupIds.size) {
                holder.groups.removeView(holder.groups.getChildAt(curr.groupIds.size))
            }
        }

        // Teachers
        var validTeacherPtr = 0
        var validTeachersSize = curr.teachers.size
        for (i in 0 until curr.teachers.size) {

            if (curr.teachers[validTeacherPtr].trim().isEmpty()) {
                validTeachersSize -= 1
                continue
            }

            val tv: TextView
            if (i < holder.teachers.childCount) {
                tv = holder.teachers.getChildAt(validTeacherPtr) as TextView
            } else {
                tv = TextView(context, null, R.attr.teacherBadgeStyle)
                holder.teachers.addView(tv)

                val lp = tv.layoutParams as FlexboxLayout.LayoutParams
                lp.rightMargin = holder.badgeSpace
                lp.topMargin = holder.badgeSpace
                tv.layoutParams = lp
            }

            val teacher = curr.teachers[validTeacherPtr]
            tv.text = teacher
            tv.setOnClickListener { onTeacherClick(teacher) }

            validTeacherPtr += 1
        }

        while (holder.teachers.childCount > validTeachersSize) {
            holder.teachers.removeView(holder.teachers.getChildAt(validTeachersSize))
        }
    }

    private fun launchInCustomTab(context: Context, _url: String) {
        var url = _url
        if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://$url"
        val uri: Uri = Uri.parse(url)

        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (e1: ActivityNotFoundException) {
            Toast.makeText(context, context.getString(R.string.unable_to_pick_activity, url), Toast.LENGTH_LONG).show()
            e1.printStackTrace()
        }
    }


}
