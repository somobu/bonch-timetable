# Как публиковать обнову

1. Обновить `versionCode` и `versionName` в `build.gradle`;
2. Закоммитить с именем формата `Version: x.y.z: Big change name`
3. Добавить тэг на гитлабе формата `vx.y.z`
4. Залить на гуглоплей

